package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strings"

	"github.com/apex/log"
	"github.com/ktr0731/go-fuzzyfinder"
	flag "github.com/spf13/pflag"
)

const (
	InfoColor    = "\033[1;34m%s\033[0m"
	NoticeColor  = "\033[1;36m%s\033[0m"
	WarningColor = "\033[1;33m%s\033[0m"
	ErrorColor   = "\033[1;31m%s\033[0m"
	DebugColor   = "\033[0;36m%s\033[0m"
)

// ................................................................................
// . Main..........................................................................
// ................................................................................
func main() {
	// Parse the flags for running
	parse_flags()

}

// ................................................................................
// . Flags ........................................................................
// ................................................................................

func parse_flags() {

	// There are three modes, which one has been selected
	interactiveQ := flag.BoolP("interactive", "i", false, "Operates via user interaction")
	translateQ := flag.BoolP("translate", "t", false, "Return matches from a sentence with dictionary entries")
	dictionaryQ := flag.BoolP("dictionary", "d", false, "fzf accross a dictionary")
	xsysQ := flag.BoolP("parse", "x", false, "Parse STDIN using x-system or h-system to convert diacritics")
	flag.Parse()

	// Run the function corresponding to the flags
	// The interactive flag will prompt for user input
	if *translateQ {
		if *interactiveQ {
			Translate(input("Please enter sx/sh phrase"))
		} else {
			Translate(gather_stdinput())
		}

	} else if *dictionaryQ {
		Dictionary()
	} else if *xsysQ {
		if *interactiveQ {
			parsex(input("Please enter sx/sh phrase"))
		} else {
			parsex(gather_stdinput())
		}

	} else {
		// If no flags were presented, use fzf to prompt the user

		commands := []string{"Dictionary", "Translate", "Replace"}

		id, err := fuzzyfinder.Find(commands, func(i int) string { return commands[i] })
		if err != nil {
			log.Error(err.Error())
			os.Exit(1)
		}

		switch id {
		case 0:
			Dictionary()
		case 1:
			Translate(input("Please Enter Phrase"))
		case 2:
			parsex(input("Please Enter Phrase"))
		}

	}

}

// ................................................................................
// . Sub Functions ................................................................
// ................................................................................

func parsex(input string) string {

	replace_dict := map[string]string{
		// "ch": "ĉ",
		// "gh": "ĝ",
		// "jh": "ĵ",
		// "sh": "ŝ",
		// "kh": "ĥ",
		"cx": "ĉ",
		"gx": "ĝ",
		"hx": "ĥ",
		"jx": "ĵ",
		"sx": "ŝ",
		"ux": "ŭ",
	}

	for key, val := range replace_dict {
		input = strings.ReplaceAll(input, key, val)
	}

	fmt.Println(input)
	return input
}

func Translate(text string) {
	for _, punct := range []string{",", ".", "?", "!", "\n"} {
		text = strings.ReplaceAll(text, punct, "")
	}
	text = strings.ToLower(text)
	text_slice := strings.Split(text, " ")
	for _, in_word := range text_slice {
		in_word = parsex(in_word)
		fmt.Printf(NoticeColor, "\n\n------------"+in_word+"-----------------------------------------------\n")
		re := regexp.MustCompile(`^[\ ]*` + in_word + `[$\s\.]`)
		for i, word := range words {
			w_en := strings.ToLower(word.English)
			w_eo := strings.ToLower(word.Esperanto)
			if re.MatchString(w_eo) || re.MatchString(w_en) {
				fmt.Printf("%s -- %s \n \t %s \n \t %s \n", words[i].English, words[i].Esperanto, words[i].Description_en, words[i].Description_eo)
			}
		}
	}

}

func Dictionary() {
	idx, err := fuzzyfinder.FindMulti(
		words,
		func(i int) string {
			return words[i].Esperanto + " -- " + words[i].English
		},
		fuzzyfinder.WithPreviewWindow(func(i, w, h int) string {
			if i == -1 {
				return ""
			}
			return fmt.Sprintf("%s -- %s \n \t %s \n \t %s", words[i].English, words[i].Esperanto, words[i].Description_en, words[i].Description_eo)
		}))
	if err != nil {
		log.Fatal(err.Error())
	}
	for _, i := range idx {
		fmt.Printf("%s -- %s \n %s \n %s", words[i].English, words[i].Esperanto, words[i].Description_en, words[i].Description_eo)
	}
}

// ................................................................................
// . Sub Sub Functoins ............................................................
// ................................................................................

func gather_stdinput() string {

	text := ""
	scan_input := bufio.NewScanner(os.Stdin)

	for scan_input.Scan() {
		line := scan_input.Text()
		text = text + line
	}

	return text

}

func input(msg string) string {
	reader := bufio.NewReader(os.Stdin)
	fmt.Printf("%s>: ", msg)

	text, err := reader.ReadString('\n')

	if err != nil {
		log.Error(err.Error())
	}

	return text
}
